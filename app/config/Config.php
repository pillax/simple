<?php
namespace app\config;

class Config {

    const DB_DEFAULT = 1;
    const DB_TEST    = 2;

    public static $databases = [
        self::DB_DEFAULT => [
            'host'  => '127.0.0.1',
            'user'  => 'root',
            'pass'  => '',
            'name'  => 'varex',
        ],
        self::DB_TEST => [
            'host'  => '127.0.0.1',
            'user'  => 'root',
            'pass'  => '',
            'name'  => 'test',
        ],
    ];
}