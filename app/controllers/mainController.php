<?php
namespace app\controllers;

use core\controller;

class mainController extends controller {

    public function landing() {
        $this->view->render('landing');
    }

    public function test() {
        die('Test page');
    }

}
