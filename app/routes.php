<?php
namespace app;

use vendor\pillax\router\src\Router;

router::init()->addRoute('GET', '/test', 'app\controllers\mainController#test');
router::init()->addRoute('GET', '/', 'app\controllers\mainController#landing');
