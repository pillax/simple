# Simple MVC

## Router
### Adding route
**Using POST and GET**
```
router::init()->addRoute('GET', '/test', 'app\controllers\testController#testme');
router::init()->addRoute('POST', '/test', 'app\controllers\testController#testme');
router::init()->addRoute('GET|POST', '/test', 'app\controllers\testController#testme');
```
**Using closure**
```
router::init()->addRoute('GET', '/test/{id:i}', function($id) { echo 'Id is ' . $id; });
```
**Regex shortcuts**
```
router::init()->addRoute('GET', '/{controller:a}/{action:a}/{id:i}', 'app\controllers\testController#testme');
```
Action testme should looks like this:
```
public function testMe($controller, $action, $id) { 
    // Parameters are in THE SAME ORDER as in the route! 
}
```
**How to order your routes**
The longest one is on the top. Example:
```
router::init()->addRoute('GET', '/news/{id:i}', 'app\controllers\testController#news');
router::init()->addRoute('GET', '/news', 'app\controllers\testController#allNews');
router::init()->addRoute('GET', '/', 'app\controllers\testController#index');
```
### Getting route
Let say we have a route:
```
router::init()->addRoute(
    'GET', 
    '/{controller:a}/{action:a}/{id:i}', 
    'app\controllers\testController#testme', 
    'fooBar'
);
```
we can take it path by calling getRoute() method:
```
$link = router::init()->getRoute('fooBar', [
     'controller' => 'news',
     'action' => 'read',
     'id' => 22,
]);
```

## Cli
Only controller can be called from shell:
Example:
```
php index.php controllerName methodName 'First param' 'second param'
```    
This will call:
```
\app\controllers\testController()->testMe('First param', 'second param');
```

## TODO
* DB - pdo, abstraction, cashe, migrations, seed
* composer/npm different libs. Check how to skip composer includes, investigate composer autoloader
* listeners implementation
* log system - log rotate
* image manipulation
* dynamic image system - storage, cache
* data validator, sanitizer - single, array, pattern-array
* Helpers
    * array
    * file
    * functions shortcuts (?)
* env file and functions
* Error hadler

 



