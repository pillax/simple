<?php
namespace core;

use vendor\pillax\response\src\Response;

class controller {
    protected $view;

    public function __construct() {
        $this->view = new view();
    }

    public function notFound(array $routerDetails=[]) {
        Response::setResponseCode(Response::RESPONSE_CODE_404);
        $this->view->render('notFound', $routerDetails);
    }
}