<?php
namespace core;

use vendor\pillax\response\src\Response;

class view {
    private $layout = 'layout';
    private $vars = [];

    public function render($template, $vars=[], $layout=null) {
        $vars = array_merge($this->vars, $vars);
        $layout = $layout ? $layout : $this->layout;

        if($vars) {
            extract($vars);
        }

        ob_start();
        include ('../app/views/' . $template . '.php');
        $templateContents = ob_get_clean();

        ob_start();
        include ('../app/views/' . $layout . '.php');
        $layoutContents = ob_get_clean();

        Response::html($layoutContents);
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout) {
        $this->layout = $layout;
    }

    /**
     * @param mixed $vars
     */
    public function setVars($vars) {
        $this->vars = $vars;
    }
}