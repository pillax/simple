<?php

use vendor\pillax\cli\src\Cli;
use vendor\pillax\router\src\Router;
use vendor\pillax\router\src\RouterException;
use vendor\pillax\simpleException\src\ExceptionParser;

require_once __DIR__ . '/../bootstrap.php';
require_once __DIR__ . '/../app/routes.php';

// process cli
if(Cli::isCli()) {
    Cli::callController($argv);
    exit;
}

// Process regular request
try {
    router::init()->match(
        isset($_GET['url']) ? $_GET['url'] : '/',
        $_SERVER['REQUEST_METHOD']
    );
} catch (RouterException $e) {
    // Display 404 page to user in case of router error
    (new \core\controller())->notFound($e->getDetails());
} catch (Throwable $e) {
    // Process other exceptions, depends on settings
    ExceptionParser::init($e)->show();
}

