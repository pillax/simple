<?php

use vendor\pillax\autoload\src\Autoload;

require_once __DIR__ . '/vendor/pillax/debug/src/debug.php';

// Register autoloading
require_once __DIR__ . '/vendor/pillax/autoload/src/Autoload.php';
Autoload::setBasepath(__DIR__ . '/');
Autoload::start();

